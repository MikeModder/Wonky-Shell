# What?
Idk tbh, it's a shell kinda thing

# Why?
Learning

# Commands
Run `help` in the shell for a list of commands

# Building
### What you need:
`go` (Go1.11+, or some way to use Go modules) and [govvv](https://github.com/JoshuaDoes/govvv)

### Do the build
`cd` to the source and run `govvv build`.